<?php

class DB{

    private $_connection;
    private static $_instance; //Singura instanta
    private $_host = "localhost";
    private $_username = "root";
    private $_password = "";
    private $_database = "Singleton";


    public static function getInstance()
    {
        if (!self::$_instance) { // Daca nu este o instanta se creeaza una
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct()
    {
        $this->_connection = new mysqli($this->_host, $this->_username,
            $this->_password, $this->_database);

        // Pentru afisarea erorilor
        if (mysqli_connect_error()) {
            trigger_error("Eroare de conectare la baza de date: " . mysqli_connect_error(),
                E_USER_ERROR);
        }
    }

    // Metoda magica clone utilizata pentru prevenirea dublarii la conectare
    private function __clone(){

    }

    public function getConnection()
    {
        return $this->_connection;
    }

}


$db = DB::getInstance();
$mysqli = $db->getConnection();
$sql_query = "SELECT nume FROM Users";
$result = $mysqli->query($sql_query);

